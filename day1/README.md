Dieser Teil des Versuchs ist identisch mit dem ersten Teil des M.PF1 Theorieversuchs. 

Das Skript und die Daten in neuster Version können unter https://gitlab.com/marvinbernhardt/m.pf1-theorieversuch/tree/master/teil1-linux heruntergeladen werden. Die Pfadangaben im Skript müssen angepasst werden.
